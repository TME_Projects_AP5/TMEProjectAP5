<?php
	// ================== ETUDIANTS =================== //
	// =========================================================================================================================
	/*
		@params: Sans parametres
		@return: void
		@Desc: Connection de la base de donnees.
	*/
	function connection_bdd(){
		try {
			$bdd = new PDO('mysql:host=127.0.0.1;dbname=tme_project', 'root', ''); // changer le mot de passe par le votre ou laissez le vide si vous en avez pas //
		} catch (Exception $e) {
			die('Erreur: '.$e->getMessage());
		}
		return $bdd;
	}
	// =========================================================================================================================

	// =========================================================================================================================
	/*
		@params: Longueur du code genere
		@return: Le code genere
		@Desc: Genere aleatoirement des codes a partir des chiffres et des lettres de l'alphabet
	*/
	function generateRandomString($length = 9) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
    	return $randomString;
	}
	// =========================================================================================================================

	//
	/**/
	function admin_existe($login, $mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM admin WHERE login=:login AND password=:mdp');
			$req->execute(array(
					'login' => $login,
					'mdp' => $mdp
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}

	}
	//

	//
	/**/
	function etudiant_existe($code, $mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM etudiant WHERE code=:code AND mdp=:mdp');
			$req->execute(array(
					'code' => $code,
					'mdp' => $mdp
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}

	}

	//

	//
	/**/
	function classe_existe($nom){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM classe WHERE nom=:nom ');
			$req->execute(array(
					'nom' => $nom
					
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}

	}
	function prof_existe($matr, $mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM prof WHERE matr=:matr AND mdp=:mdp');
			$req->execute(array(
					'matr' => $matr,
					'mdp' => $mdp
				));
		if ($req->fetch()) {
			return 1;
		}
		else{
			return 0;
		}
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}

	}
	//

	// =========================================================================================================================
	/*
		Je commenterai par la suite ... toute facon c comprehensible.

	*/

	function ajouter_etudiant($nom, $prenom, $ddn, $adr, $numtel, $nom_classe, $mdp){
		$bdd = connection_bdd();
		$code = generateRandomString();
		try {
			$req = $bdd->prepare('INSERT INTO etudiant (nom, prenom, ddn, adr, numtel, nom_classe, code, mdp) VALUES(:nom, :prenom, :ddn, :adr, :numtel, :nom_classe, :code, :mdp)');
			$req->execute(array(
					'nom' => $nom,
					'prenom' => $prenom,
					'ddn' => $ddn,
					'adr' => $adr,
					'numtel' => $numtel,
					'nom_classe' => $nom_classe,
					'code' => $code,
					'mdp' => $mdp
				));
		return $code;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}
	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/

	function supprimer_etudiant($code){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM etudiant WHERE code=?');
			$req->execute(array($code));
			return $code;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}
	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function modifier_mdp_etudiant($code, $nouveau_mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('UPDATE etudiant SET mdp=:nouveau_mdp WHERE code=:code');
			$req->execute(array(
				'nouveau_mdp' => $nouveau_mdp,
				'code' => $code
				));
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));			
		}
	}


	// =========================================================================================================================


	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function compter_etudiants(){
		$bdd = connection_bdd();
		$result = 0;
		try {
			$res = $bdd->query('SELECT COUNT(*) AS nbre_etudiants FROM etudiant');
			while ($donnees = $res->fetch()) {
				$result = $donnees['nbre_etudiants'];
			}
			return $result;
			$res->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));			
		}

	}


	// =========================================================================================================================

	// ================== PROFESSEURS =================== //
	// =========================================================================================================================
	/*

	*/

	function ajouter_prof($nom, $prenom, $adr, $numtel,  $mdp){
		$bdd = connection_bdd();
		$matr = generateRandomString();
		try {
			$req = $bdd->prepare('INSERT INTO prof (nom, prenom, adr, numtel, matr, mdp) VALUES(:nom, :prenom, :adr, :numtel, :matr, :mdp)');
			$req->execute(array(
					'nom' => $nom,
					'prenom' => $prenom,
					'adr' => $adr,
					'numtel' => $numtel,
					'matr' => $matr,
					'mdp' => $mdp
				));
		return $matr;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}

	// =========================================================================================================================

	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function supprimer_prof($matr){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM prof WHERE code=?');
			$req->execute(array($matr));
			return $matr;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}

	// =========================================================================================================================

	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function modifier_mdp_prof($matr, $nouveau_mdp){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('UPDATE prof SET mdp=:nouveau_mdp WHERE matr=:matr');
			$req->execute(array(
				'nouveau_mdp' => $nouveau_mdp,
				'matr' => $matr
				));
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));			
		}
	}

	// =========================================================================================================================

	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function compter_profs(){
		$bdd = connection_bdd();
		$result = 0;
		try {
			$res = $bdd->query('SELECT COUNT(*) AS nbre_profs FROM prof');
			while ($donnees = $res->fetch()) {
				$result = $donnees['nbre_profs'];
			}
			return $result;
			$res->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));			
		}

	}

	// =========================================================================================================================

	// =========================================================================================================================

	// ================== CLASSES =================== //
	// =========================================================================================================================
	/*

	*/
	function ajouter_classe($nom, $effectif){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO classe (nom, effectif) VALUES(:nom, :effectif)');
			$req->execute(array(
					'nom' => $nom,
					'effectif' => $effectif,
				));
		return $nom;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}

	// =========================================================================================================================

	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function supprimer_classe($nom){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM classe WHERE nom=?');
			$req->execute(array($nom));
			return $nom;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}

	// =========================================================================================================================

	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function compter_classes(){
		$bdd = connection_bdd();
		$result = 0;
		try {
			$res = $bdd->query('SELECT COUNT(*) AS nbre_classes FROM classe');
			while ($donnees = $res->fetch()) {
				$result = $donnees['nbre_classes'];
			}
			return $result;
			$res->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));			
		}

	}

	// =========================================================================================================================

	// =========================================================================================================================

	// ================== MATIERES =================== //
	// =========================================================================================================================
	/*

	*/
	function ajouter_matiere($nom, $nom_classe, $matr_prof){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO matiere (nom, nom_classe, matr_prof) VALUES(:nom, :nom_classe, :matr_prof)');
			$req->execute(array(
					'nom' => $nom,
					'nom_classe' => $nom_classe,
					'matr_prof' => $matr_prof
				));
		return $nom;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}

	// =========================================================================================================================

	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function supprimer_matiere($nom){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM matiere WHERE nom=?');
			$req->execute(array($nom));
			return $nom;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}

	// =========================================================================================================================

	// ================== EXAMENS =================== //
	// =========================================================================================================================
	/*

	*/
	function ajouter_examen($code, $nom_matiere){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO examen (code, nom_matiere) VALUES(:code, :nom_matiere)');
			$req->execute(array(
					'code' => $code,
					'nom_matiere' => $nom_matiere
				));
		return $code;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}

	// =========================================================================================================================

	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function supprimer_examen($code){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM examen WHERE code=?');
			$req->execute(array($code));
			return $code;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}

	// =========================================================================================================================
	// ================== QUESTIONS =================== //
	// =========================================================================================================================
	/*

	*/
	function ajouter_question($libelle, $duree, $type, $code_examen){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO question (libelle, duree, type, code_examen) VALUES(:libelle, :duree, :type, :code_examen)');
			$req->execute(array(
					'libelle' => $libelle,
					'duree' => $duree,
					'type' => $type,
					'code_examen' => $code_examen
				));
		return $libelle;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
		
	}

	// =========================================================================================================================

	// =========================================================================================================================

	// =========================================================================================================================
	/*

	*/
	function supprimer_question($id){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('DELETE FROM question WHERE id=?');
			$req->execute(array($id));
			return $id;
			$req->closeCursor();
		} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	
		}
	}
	 function ajouter_correction_qr($id_question, $commentaire, $points_attribues){
	 	$bdd = connection_bdd();
	 	try {
	 		$req = $bdd->prepare('INSERT INTO correction_qr (id_question, commentaire, points_attribues) VALUES (:id_question, :commentaire, :points_attribues)');
	 		$req->execute(array(
	 				'id_question' => $id_question,
	 				'commentaire' => $commentaire,
	 				'points_attribues' => $points_attribues
	 			));
	 		
	 	} catch (Exception $e) {
			die('Erreur: ' .$e->getMessage());
			die(print_r($bdd->errorInfo()));	 		
	 	}
	}

	// =========================================================================================================================

	//
	/*

	*/
	function ajouter_reponse_etudiant($contenu, $id_question, $code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO reponse_etudiant (contenu, nbre_points, id_question, code_etudiant) VALUES(:contenu, 0, :id_question, :code_etudiant)');
			$req->execute(array(
					'contenu' => $contenu,
					'id_question' => $id_question,
					'code_etudiant' => $code_etudiant
				));
		return $contenu;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}
	//
	
	//
	/*

	*/
	function ajouter_reponse_prof($contenu, $nbre_points, $id_question){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO reponse_prof (contenu, nbre_points, id_question) VALUES(:contenu, :nbre_points, :id_question)');
			$req->execute(array(
					'contenu' => $contenu,
					'nbre_points' => $nbre_points,
					'id_question' => $id_question
				));
		return $contenu;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	function ajouter_correction($note, $code_etudiant){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('INSERT INTO correction (note, code_etudiant) VALUES(:note, :code_etudiant)');
			$req->execute(array(
					'note' => $note,
					'code_etudiant' => $code_etudiant
				));
		return $note;
		$req->closeCursor();


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}
	}

	// MANIPULATION AVANCEE

	// 
	/*
	*/
	function liste_etudiants($nom_classe){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT * FROM etudiant WHERE nom_classe = :nom_classe');
			$req->execute(array(
					'nom_classe' => $nom_classe
				));


		$tab_etud = $req->fetchAll();
		$req->closeCursor();
		return $tab_etud;


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}

	}


	//

	// 
	/*
	*/
	function liste_reponses_etudiant($code_etudiant, $id_question, $code_examen){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT reponse_etudiant.contenu FROM reponse_etudiant, question, examen WHERE code_etudiant=:code_etudiant AND id_question=:id_question AND code_examen=:code_examen');
			$req->execute(array(
					'code_etudiant' => $code_etudiant,
					'id_question' => $id_question,
					'code_examen' => $code_examen
				));

		$liste_reponses = $req->fetchAll();
		$req->closeCursor();
		return $liste_reponses;


		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));
		}

	}

	
	//
	// 
	/*
	*/
	// function ajouter_examen_fini($code_etudiant, $code_examen, $id_question, $repon){
	// 	$bdd = connection_bdd();
	// 	try {
	// 		$req = $bdd->prepare('INSERT INTO examen_fini (code_etudiant, code_examen, id_question) VALUES (:code_etudiant, :code_examen, :id_question)');
	// 		$req->execute(array(
	// 				'code_etudiant' => $code_etudiant,
	// 				'code_examen' => $code_examen,
	// 				'id_question' => $id_question
	// 			));
	// 	$req->closeCursor();


	// 	} catch (Exception $e) {
	// 		die('Erreur: '. $e->getMessage);
	// 		die(print_r($bdd->errorInfo()));
	// 	}

	// }

	
	//
	function liste_questions_examen($code_examen){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT type, libelle FROM question WHERE code_examen=:code_examen');
			$req->execute(array(
					'code_examen' => $code_examen
				));
			$liste_questions = $req->fetchAll();
			return $liste_questions;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}

	function liste_classes_enseignees($matr_prof){
		$bdd = connection_bdd();
		try {
			$req = $bdd->prepare('SELECT nom_classe FROM prof WHERE matr=:matr');
			$req->execute(array(
					'matr' => $matr_prof
				));
			while ($req->fetch()){
				$donnees = $req->fetch();
			}
			return $donnees;
			$req->closeCursor();
			
		} catch (Exception $e) {
			die('Erreur: '. $e->getMessage);
			die(print_r($bdd->errorInfo()));			
		}
	}
?>