<!DOCTYPE html>
<html>
<head>
	<title>Accueil</title>
	<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<!-- Corps principal -->
	<div class="conteneur-corps">
		<!-- Le corps du haut qui contient le header(menu + logo) -->
		<div class="corps-du-haut">			
			<header class="conteneur-entete">	
				<img class="logo-image" src="images/logo.jpg" alt="Logo">	
				<nav class="conteneur-menu">
					<ul class="menu-liste-onglets">
						<li>Accueil</li>
						<li>Informations</li>
						<li>Aide</li>
						<li>A propos</li>
						<li>Connexion</li>
					</ul>
				</nav>
			</header>
			<h1>Modernisation de l'Education a l' Ecole Superieure Polytechnique de Dakar</h1>
			<p>Pour remplir cet espace qui n'est pas cense etre vide, je mets du texte insignifiant. Normalement il doit y avoir un joli background image et un peu de texte comme celui que je suis entrain de rediger betement.</p>
		</div>
		<!-- Fin corps du haut -->

		<!-- Le corps du bas qui contient les espaces de connexion -->
		<div class="corps-du-bas">
			<h2 class="titre-bienvenue">Bienvenue a Working Space <abbr title="Ecole Superieure Polytechnique">ESP</abbr></h2>
				<ul>
					<li>
						<div class="panel"><a href="espace_etudiant.php"><h4>Espace Etuidiants</h4></a></div>
						<p>
							Description: Lorem ipsum dolor sit amet
						</p>
					</li>
					<li>
						<div class="panel"><a href="espace_prof.php"><h4>Espace Professeurs</h4></a></div>
						<p>
							Description: Lorem ipsum dolor sit amet
						</p>
					</li>
					<li>
						<div class="panel"><a href="espace_admin.php"><h4>Espace Administrateur</h4></a></div>
						<p>
							Description: Lorem ipsum dolor sit amet
						</p>
					</li>
				</ul>
		</div>		
		<!-- Fin corps du bas -->
	</div>
	<!-- Fin corps principal -->
	
	<!-- Pied de page -->
	<footer class="conteneur-pied-de-page">
		<h3>Contacts</h3>
		<ul>
			<li>Telephone</li>
			<li>E-mail</li>
			<li>Follow us</li>
		</ul>
	</footer>
	<!-- Fin pied de page -->

</body>
</html>